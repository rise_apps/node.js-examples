import httpStatus from 'http-status';
import passwordHash from 'password-hash';
import APIError from '../helpers/APIError';
import BarAdmin from '../models/barAdmin.model'
import User from '../models/user.model'

let barAdmCtrl = {}

barAdmCtrl.loadAdmin = function (req, res, next, id) {
    const request = req;
    BarAdmin.get(id, req.bar.id)
        .then(barAdmin => {
            request.barAdmin = barAdmin;
            return next();
        })
        .catch(e => next(e));
}


barAdmCtrl.addAdminToBar = function (req, res, next) {
    let userId = 0
    if (!req.body.userId){
        const user = new User({
            name: req.body.name,
            surname: req.body.surname,
            mobileNumber: req.body.mobileNumber,
            email: req.body.email,
            login: req.body.login,
            password: passwordHash.generate(req.body.password),
            role: 'administrator'
        });
        user.save()
            .then(newUser => {
                return BarAdmin.addBarAdmin(newUser.insertId, req.bar.id)
            })
            .then(saved => {
                return BarAdmin.adminsList(req.bar.id)
            })
            .then(admins => {
                res.json(admins)
            })
            .catch(e => next(e));
    }else {
        userId = req.body.userId
        User.update(userId, {role: 'administrator'})

        BarAdmin.addBarAdmin(userId, req.bar.id)
            .then(saved => {
                return BarAdmin.adminsList(req.bar.id)
            })
            .then(admins => {
                res.json(admins)
            })
            .catch(e => next(e));
    }
}

barAdmCtrl.getAdmins = function (req, res, next) {
    BarAdmin.adminsList(req.bar.id)
        .then(admins => {
            res.json(admins)
        })
        .catch(e => next(e));
}

barAdmCtrl.getBarAdmin = function (req, res, next) {
    res.json(req.barAdmin);
}

barAdmCtrl.editBarAdmin = function (req, res, next) {
    if (!req.barAdmin) throw new APIError('Bar doesn\'t exists!', 'ExistError', httpStatus.NOT_FOUND)
    BarAdmin
        .updateBarAdmin(req.barAdmin.id, req.bar.id, req.body)
        .then(updatedBar => res.json(updatedBar))
        .catch(e => next(e));
}

barAdmCtrl.removeAdminFromBar = function (req, res, next) {
    const userId = req.url.split('/')[1]
    User.setUserRole(userId, 'customer')
        .catch(e => next(e))

    BarAdmin.removeBarAdmin(userId, req.bar.id)
        .then(admins => {
            res.json(admins)
        })
        .catch(e => next(e));
}

export default barAdmCtrl
