import Promise from 'bluebird';
import db from '../../dbconnection'
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

const tableName = 'users'
class User {

    constructor(user){
        this.user = user
    }

    static get(id){
        return new Promise(function(resolve, reject) {
            return db.query('SELECT * FROM '+tableName+' WHERE id = ?', [id], function (error, results) {
                if (error) reject(error);
                if (!results[0]) reject(new APIError(`No such user exists!`, 'ExistError', httpStatus.NOT_FOUND, true))
                else{
                    delete results[0]['password']
                    resolve(results[0])
                }
            } )
        })
    }

    static getUserByLogin(login) {
        return new Promise(function(resolve, reject) {
            return db.query('SELECT * FROM '+tableName+' WHERE login = ?', [login], function (error, results) {
                if (error) reject(error);
                if (!results[0]) reject(new APIError('Authentication error. User doesn\'t exist', 'ExistError', httpStatus.NOT_FOUND, true))
                resolve(results[0])
            } )
        })
            .then(user => {
                return new Promise(function(resolve, reject) {
                    return db.query('SELECT roleName FROM userRoles WHERE id = ?', [user.role], function (error, results) {
                        if (error) reject(error);
                        if (!results[0]) reject(new APIError('Authentication error. User doesn\'t exist', 'ExistError', httpStatus.NOT_FOUND, true))
                        user.role = results[0].roleName
                        resolve(user)
                    } )
                })
            })
    }

    static getUserRoles(){
        return new Promise(function(resolve, reject) {
            return db.query('SELECT * FROM userRoles', function (error, results) {
                if (error) reject(error);
                resolve(results)
            } )
        })
    }

    static getUserRoleId(roleName){
        if (roleName === 'all') return null
        return new Promise(function(resolve, reject) {
            return db.query('SELECT id FROM userRoles WHERE roleName = ?', [roleName], function (error, results) {
                if (error) reject(error);
                return resolve(results[0].id)
            } )
        })
    }

    static remove(id){
        return new Promise(function(resolve, reject) {
            return db.query('DELETE FROM '+tableName+' WHERE id = ?', [id], function (error, results) {
                if (error) reject(error);
                resolve(results)
            } )
        })
    }

    static list({role = 'all' } = {}){
        return new Promise(function(resolve, reject) {
            return resolve(User.getUserRoleId(role))
        })
            .then(roleId => {
                return (!roleId) ? '' : ' WHERE role= \''+roleId+'\''
            })
            .then(sqlWhere => {
                let sql = 'SELECT id,login,name,surname,mobileNumber,email,role FROM '+tableName+sqlWhere
                return new Promise(function(resolve, reject) {
                    return db.query(sql, (error, results, fields) => {
                        if (error) reject(new APIError('Cant select users from DB', error.name, httpStatus.NOT_FOUND, true))
                        resolve(results)
                    })
                })
            })
    }

    static setUserRole(id, role){
        return this.getUserRoleId(role)
            .then(roleId => {
                return new Promise(function(resolve, reject) {
                    return db.query('UPDATE '+tableName+' SET role = ? WHERE id = ?', [roleId, id], (error, results, fields) => {
                        if (error) {
                            switch (error.code){
                                case 'ER_PARSE_ERROR':
                                    reject(new APIError('API recieved wrong fields!', "Wrong property", httpStatus.BAD_REQUEST, true))
                                    break
                                case 'ER_DUP_ENTRY':
                                    reject(new APIError(error.sqlMessage, "Duplicate exists field", httpStatus.BAD_REQUEST, true));
                                    break
                                default:
                                    reject(error);
                            }
                        }
                        resolve(results)
                    })
                })
                .then(results => {
                    return this.get(id)
                })
            })
    }

    static update(id, obj){
        if (obj.role){
            delete obj.role
        }
        let fieldsToUpdate = ''
        Object.keys(obj).map(function(key, index) {
            let coma = (Object.keys(obj).length === index + 1)? '' : ','
            fieldsToUpdate += key +' = \''+ obj[key] +'\'' + coma
        });
        let sql = 'UPDATE '+tableName+' SET '+fieldsToUpdate+' WHERE id = '+id
        return new Promise(function(resolve, reject) {
            return db.query(sql, (error, results, fields) => {
                if (error) {
                    switch (error.code){
                        case 'ER_PARSE_ERROR':
                            reject(new APIError('API recieved wrong fields!', "Wrong property", httpStatus.BAD_REQUEST, true))
                            break
                        case 'ER_DUP_ENTRY':
                            reject(new APIError(error.sqlMessage, "Duplicate exists field", httpStatus.BAD_REQUEST, true));
                            break
                        default:
                            reject(error);
                    }
                }
                resolve(results)
            })
        }).then(results => {
            return this.get(id)
        })
    }

    save(){
        const role = this.user.role ? this.user.role : 'customer'
        return this.setUserRole(role)
            .then(data => {
                let sql = "INSERT INTO "+tableName+
                    " (name, surname, mobileNumber, email, login, password, role)" +
                    " VALUES ?"
                let values = [[...Object.values(data)]]
                return new Promise(function(resolve, reject) {
                    return db.query(sql, [values], function (error, results,fields) {
                        if (error) reject(new APIError(error.sqlMessage, "Duplicate exists field", httpStatus.BAD_REQUEST));
                        resolve(results)
                    } )
                })
            })
    }

    selectById(id){
        return db.query('SELECT * FROM '+tableName+' WHERE id = ?', [id], function (error, results) {
            if (error) return error;
            delete results[0]['password']
            return results[0]
        } )
    }

    setUserRole(roleName){
        return User.getUserRoleId(roleName)
            .then(id => {
            this.user.role = id
            return this.user
        })
    }

    presave(params){
        return new Promise(function(resolve, reject) {
            return db.query('SELECT login FROM '+tableName+' WHERE login = ?', [params.login], function (error, results) {
                if (error) reject(error);
                resolve(results)
            } )
        })
    }

}

export default User