import Promise from 'bluebird';
import db from '../../dbconnection'
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

const tableName = 'filters'

class Filter {

    static addFilter(filterName, filterType, filterLength){
        let sql = "ALTER TABLE "+tableName+" ADD "+filterName+" "+filterType+"("+filterLength+")"
        return new Promise(function(resolve, reject) {
            return db.query(sql, function (error, results) {
                if (error) reject(new APIError(error.sqlMessage, "Something went wrong!", httpStatus.BAD_REQUEST));
                resolve(results)
            } )
        })
            .then(results => this.filterList())

    }

    static filterList(){
        let sql = "SELECT ORDINAL_POSITION as position, " +
            "COLUMN_NAME as name, COLUMN_TYPE as type " +
            "FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '"+tableName+"'"
        return new Promise(function(resolve, reject) {
            return db.query(sql, function (error, results) {
                if (error) reject(new APIError(error.sqlMessage, "Something went wrong!", httpStatus.BAD_REQUEST));
                results.splice(0, 1);
                resolve(results)
            } )
        })
    }

    static removeFilter(filterName){
        let sql = "ALTER TABLE "+tableName+" DROP "+filterName
        return new Promise(function(resolve, reject) {
            return db.query(sql, function (error, results) {
                if (error) reject(new APIError(error.sqlMessage, "Something went wrong!", httpStatus.BAD_REQUEST));
                resolve(results)
            } )
        })
            .then(results => this.filterList())

    }

    static updateFilter(filter){
        let sql = "ALTER TABLE "+tableName+" CHANGE "+filter.name+" " +
                    filter.newName+" "+filter.type+"("+filter.length+")"

        return new Promise(function(resolve, reject) {
            return db.query(sql, function (error, results) {
                if (error) reject(new APIError(error.sqlMessage, "Bad request", httpStatus.BAD_REQUEST));
                resolve(results)
            } )
        })
            .then(results => this.filterList())

    }

}

export default Filter