import Promise from 'bluebird';
import db from '../../dbconnection'
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import geo from '../helpers/geocoding'

const tableName = 'bars'

class Bar {
    constructor(bar){
        this.bar = bar
    }

    static get(id){
        return new Promise(function(resolve, reject) {
            return db.query('SELECT * FROM '+tableName+' WHERE id = ?', [id], function (error, results) {
                if (error) reject(error);
                if (!results[0]) reject(new APIError('No such bar exists!', 'ExistError', httpStatus.NOT_FOUND, true))
                else{
                    resolve(results[0])
                }
            } )
        }).then(bar => {
            return new Promise(function(resolve, reject) {
                return db.query('SELECT * FROM filters WHERE id = ?', [bar.filters], function (error, results) {
                    if (error) reject(error);
                    if (!results[0]) bar.filters = {}
                    else{
                        bar.filters = results[0]
                    }
                    resolve(bar)
                } )
            })
        })
    }

    static remove(id){
        return new Promise(function(resolve, reject) {
            return db.query('DELETE FROM '+tableName+' WHERE id = ?', [id], function (error, results) {
                if (error) reject(error);
                resolve(results)
            } )
        })
    }

    static update(id, obj){
        let sql = 'UPDATE '+tableName+', filters SET ',
            fieldsToUpdate = '',
            sqlWhere = ' WHERE '
        if (obj.filters){
            Object.keys(obj.filters).map(function(key, index) {
                let coma = (Object.keys(obj.filters).length === index + 1 && Object.keys(obj).length <= 1)? '' : ','
                fieldsToUpdate += 'filters.'+ key +' = \''+ obj.filters[key] +'\'' + coma
            });
            delete obj.filters
            sqlWhere += 'filters.id = bars.filters AND '
        }
        Object.keys(obj).map(function(key, index) {
            let coma = (Object.keys(obj).length === index + 1)? '' : ','
            fieldsToUpdate += tableName+'.'+ key +' = \''+ obj[key] +'\'' + coma
        });
        sqlWhere += tableName+'.id = '+id
        sql += fieldsToUpdate+sqlWhere
        return new Promise(function(resolve, reject) {
            return db.query(sql, (error, results, fields) => {
                if (error) {
                    switch (error.code){
                        case 'ER_PARSE_ERROR':
                            reject(new APIError('API recieved wrong fields!', "Wrong property", httpStatus.BAD_REQUEST, true))
                            break
                        case 'ER_DUP_ENTRY':
                            reject(new APIError(error.sqlMessage, "Duplicate exists field", httpStatus.BAD_REQUEST, true));
                            break
                        default:
                            reject(error);
                    }
                }
                resolve(results)
            })
        }).then(results => {
            return this.get(id)
        })
    }

    static list({limit = 50, lat = null, lng = null, distance = 5, ...filters } = {}) {
        return new Promise((resolve,reject) => {
            if (lat&&lng){
                geo.reverseGeocode(lat, lng, (err,data) => {
                    data.results.map((el) => {
                        if (el.types.indexOf('locality') > -1){
                            resolve(el.address_components[0].long_name)
                        }
                    })
                })
            } else {
                resolve('')
            }
        }) .then(city => {
                let sqlWhere = ' WHERE '
                if(city){
                    sqlWhere += tableName+'.city = \''+city+'\' AND '
                }
                if (filters){
                    Object.keys(filters).map(function(key, index) {
                        sqlWhere += 'filters.'+ key +' = \''+ filters[key] +'\'' + ' AND '
                    });
                }
                sqlWhere += tableName+'.status=\'approved\''
                let sql = 'SELECT '+tableName+'.* FROM '+tableName+' LEFT JOIN filters ON (filters.id = '+tableName+'.filters)'+sqlWhere
                return new Promise(function(resolve, reject) {
                    return db.query(sql, (error, results, fields) => {
                        if (error) reject(new APIError('Cant select bars from DB', error.name, httpStatus.NOT_FOUND, true))
                        resolve(results)
                    })
                })
                .then(bars => {
                    if(lat&&lng){
                        let results = []
                        bars.map(bar => {
                            const R = 6371; // kilometres
                            const φ1 = Math.radians(lat);
                            const φ2 = Math.radians(bar.lat);
                            const Δφ = Math.radians((bar.lat-lat));
                            const Δλ = Math.radians((bar.lng-lng));

                            const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                                Math.cos(φ1) * Math.cos(φ2) *
                                Math.sin(Δλ/2) * Math.sin(Δλ/2);
                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                            const d = R * c;
                            if (d < distance) {
                                let nearBar = Object.assign({distantion: +d*1000}, bar)
                                results.push(nearBar)
                            }
                        })

                        return results;
                    } else {
                        return bars
                    }

                });
        })
    }

    static superadminBarList({status = 'all'}) {
        const sqlWhere = status == 'all' ? '' : ` WHERE status = '${status}'`
        return new Promise((resolve,reject) => {
            return db.query('SELECT * FROM '+tableName+sqlWhere, (error, results, fields) => {
                if (error) reject(new APIError('Cant select users from DB', error.name, httpStatus.NOT_FOUND, true))
                resolve(results)
            })
        })
    }

    static nearestBar(lat, lng) {
        return new Promise((resolve,reject) => {
            geo.reverseGeocode(lat, lng, (err,data) => {
                data.results.map((el) => {
                    if (el.types.indexOf('locality') > -1){
                        resolve(el.address_components[0].long_name)
                    }
                })
            })
        }) .then(city => {
            let sqlWhere = ' WHERE '

            sqlWhere += 'city = \''+city+'\' AND '

            sqlWhere += 'status=\'approved\''

            let sql = 'SELECT '+tableName+'.* FROM '+tableName+' '+sqlWhere
            return new Promise(function(resolve, reject) {
                return db.query(sql, (error, results) => {
                    if (error) reject(new APIError('Cant select bars from DB', error.name, httpStatus.NOT_FOUND, true))
                    resolve(results)
                })
            })
                .then(bars => {
                    if(lat&&lng){
                        bars.map(bar => {
                            const R = 6371; // kilometres
                            const φ1 = Math.radians(lat);
                            const φ2 = Math.radians(bar.lat);
                            const Δφ = Math.radians((bar.lat-lat));
                            const Δλ = Math.radians((bar.lng-lng));

                            const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                                Math.cos(φ1) * Math.cos(φ2) *
                                Math.sin(Δλ/2) * Math.sin(Δλ/2);
                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                            const d = R * c * 1000;
                            if (d <= 20) {
                                let nearBar = Object.assign({distantion: d}, bar)
                                return nearBar
                            }
                        })
                        return null
                    } else {
                        return null
                    }

                });
        })
    }

    save(){
        let bar = this.bar
        let fieldsToSave = ''
        Object.keys(bar).map(function(key, index) {
            let coma = (Object.keys(bar).length === index + 1)? '' : ','
            fieldsToSave += key + coma
        });
        let sql = "INSERT INTO "+tableName+
            " ("+fieldsToSave+")" +
            " VALUES ?"
        let values = [[...Object.values(bar)]]
        return new Promise(function(resolve, reject) {
            return db.query(sql, [values], function (error, results) {
                if (error) reject(new APIError(error.sqlMessage, "Duplicate exists field", httpStatus.BAD_REQUEST));
                resolve(results)
            } )
        })
    }
}

Math.radians = function(degrees) {
    return degrees * Math.PI / 180;
};

export default Bar